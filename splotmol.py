#!/usr/bin/python3
# 3D visualization of molecule and vectors

import sys, os
import numpy as np
from lib.chemonklib import ccamol, splotmol, initfpm, showfpm, draw_mol, draw_pol, draw_vec, draw_plane, draw_cylinder
from lib.tuttilib import rotmat, sph2cart
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("cca", type=str, help="Filename of cca file", nargs='?')
parser.add_argument("--ivec", type=str, help="Vector to be rotated x,y,z")
parser.add_argument("--fvec", type=str, default="0,0,1", help="Vector to be aligned with x,y,z")
parser.add_argument("--cube", action='store_true', help="Draw a cube aligned ")
parser.add_argument("--pmod", type=str, default="mlab", help="Plotting mode (mlab/plt)")
parser.add_argument("--pang", type=str, help="Angles of the polarization vector in degree (beta, alpha)")
parser.add_argument("--eang", type=str, help="Angles of the electron momentum in degree (theta, phi)")
parser.add_argument("-r", "--ref", action='store_true', help="Plot referential")
parser.add_argument("-p", "--plane", type=str, help="Coordinates of 3 points (x1,y1,z1_x2,y2,z2_x3,y3,z3")
parser.add_argument("-a", "--indat", action='store_true', help="Use index of atoms instead of coordinates in plane definition")
parser.add_argument("--veclab", type=str, default="E", help="Label for the polarization vector")
parser.add_argument("--momlab", type=str, default="e⁻", help="Label for the electron momentum")
parser.add_argument("--reflab", type=str, default="x,y,z", help="Label for the reference axis (x,y,z)")
parser.add_argument("--mollength", type=float, default=1., help="Length of molecular bonds")
parser.add_argument("--reflength", type=float, default=0.3, help="Length of reference axis")
parser.add_argument("--veclength", type=float, default=0.7, help="Length of polarization vector")
parser.add_argument("--momlength", type=float, default=0.7, help="Length of electron momentum")
parser.add_argument("--cubelength", type=float, default=1, help="Length of cube")
parser.add_argument("--planelength", type=float, default=1, help="Length of plane")
parser.add_argument("--halfvec", action='store_true', help="Draw only half of polarization vector")
args = parser.parse_args()

deg2rad = np.pi/180.
antial=False

pmod = args.pmod

if args.cca:
    mol = ccamol(args.cca)
    mol.readcca()
    rmax = mol.rmax
else:
    mol = None
    rmax = 1
    if args.indat:
        print("Error! Cannot use indat option without cca file!")
        exit()

if args.pang:
    vp = sph2cart([1.]+[float(i)*deg2rad for i in args.pang.split(",")])
    
if args.eang:
    ve = sph2cart([1.]+[float(i)*deg2rad for i in args.eang.split(",")])

if args.ivec:
    vi = np.array(args.ivec.split(","), dtype=float)
    vf = np.array(args.fvec.split(","), dtype=float)
    
    if not mol is None:
        mol.carts = rotmat(vi,vf,mol.carts[:,0],mol.carts[:,1],mol.carts[:,2], aform=True)
    
    if args.pang:
        vp = rotmat(vi,vf,vp[0],vp[1],vp[2], aform=True)

fpm, plt = initfpm(pmod)

if not mol is None:
    draw_mol(fpm, mol, fac=0.5/args.mollength, npt=500, pmod=pmod, antial=antial)

if args.pang:
    if args.halfvec:
         draw_vec(fpm, [vp], rmax=args.veclength*rmax, dt=0.1*rmax, pr=0.03*0.3/args.veclength, color=[(1.,0.,1.)], text=[args.veclab], pmod=pmod, antial=antial)
    else:
        draw_pol(fpm, vp, rmax=args.veclength*rmax, dt=0.1*rmax, pr=0.03*0.3/args.veclength, text=args.veclab, pmod=pmod, antial=antial)

if args.eang:
    if args.halfvec:
         draw_vec(fpm, [ve], rmax=args.momlength*rmax, dt=0.1*rmax, pr=0.03*0.3/args.momlength, color=[(0,1.,1.)], text=[args.momlab], pmod=pmod, antial=antial)

if args.ref:
    draw_vec(fpm, [[1,0,0], [0,1,0], [0,0,1]], rmax=args.reflength*rmax, dt=0.1*rmax, pr=0.03*0.3/args.reflength, color=[(1,0,0), (0,1,0), (0,0,1)], text=args.reflab.split(","), pmod=pmod, antial=antial)

if args.cube > 0:
    
    vert1 = np.array([[1,-1,-1], [1,-1,-1], [1,-1,-1], [1,-1,1], [1,-1,1], [1,1,-1], [1,1,-1], [1,1,1], [-1,-1,1], [-1,-1,1], [-1,1,-1], [-1,1,-1]])
    vert2 = np.array([[1,-1,1], [1,1,-1], [-1,-1,-1], [1,1,1], [-1,-1,1], [1,1,1], [-1,1,-1], [-1,1,1], [-1,1,1], [-1,-1,-1], [-1,1,1], [-1,-1,-1]])
    
    for i in range(len(vert1)):
        draw_cylinder(fpm, vert1[i]*rmax*args.cubelength, vert2[i]*rmax*args.cubelength, 0.05, 500, alpha=0.5,color=(0,1.,1.), pmod=pmod, antial=antial)

if args.plane:
    vert = [[float(j) for j in i.split(",")] for i in args.plane.split("_")]
    if len(vert) != 3:
        print("Error! Bad number of points for vert argument!")
        sys.exit()
    
    if args.indat:
        alim = True
        xyzs = np.zeros((3,3))
        for i in range(3):
            nat = len(vert[i])
            for j in range(nat):
                xyzs[i,:] = xyzs[i,:]+mol.carts[int(vert[i][j]),:]/nat
        vert = np.copy(xyzs)
    else:
        alim = False
        vert = np.array(vert)*rmax*args.planelength
    
    assert vert.shape == (3,3), "Error! Bad array size for vert!"

    draw_plane(fpm, vert, 500, alim=True, alpha=0.5, color=(1, 1, 1), pmod=pmod, antial=antial)

showfpm(fpm, plt)
