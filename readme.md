# Readme

## About splotmol

**splotmol** is a Python code to visualize in 3D the molecular structure (given as input by a .cca file) and some geometrical components such as planes and vectors. Rotation of the coordinates can be performed by defining aligning axes.

**Example of 3D visualization of CF4 molecule (+ annotations):**
<img src="examples/cf4-mol_pv-39.73-45_c.png" width="40%">

## Installation

Requirements:
1. Python 3.0 and higher versions
2. Numpy
3. Matplotlib or mayavi
4. Scipy
5. openbabel
6. mendeleev

## Inputs

The code requires to read a .cca file which is based on the .xyz file format with additional parameters to define the connectivity:

- 1st line: Number-of-atoms
- 2nd line: Energy (irrelevant)
- 3rd line: Atom(1)_label Atom(1)_x-coordinate Atom(1)_y-coordinate Atom(1)_z-coordinate
- 4th line: Atom(2)-label Atom(2)_x-coordinate Atom(2)_y-coordinate Atom(2)_z-coordinate
- ...
- N+2 line: Atom(N)_label Atom(N)_x-coordinate Atom(N)_y-coordinate Atom(N)_z-coordinate
- N+3 line: List of atom indexes to which Atom(1) is connected
- N+4 line: List of atom indexes to which Atom(2) is connected
- ...
- 2N+2 line: List of atom indexes to which Atom(N) is connected

## Licence

This code is under GNU General Public License (GPL) version 3
